DS=mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.recon.AOD.e6337_s3126_r10201
TAG=v0


pathena --trf "Reco_tf.py --inputAODFile /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/DerivationFrameworkART/AOD.14795494._005958.pool.root.1 --outputDAODFile art.pool.root --reductionConf FTAG1 --maxEvents -1 --preExec 'rec.doApplyAODFix.set_Value_and_Lock(True);from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \"BTagCalibRUN12-08-47\"; BTaggingFlags.Do2019Retraining = True; from BTagging.JetCollectionToTrainingMaps import postTagDL2JetToTrainingMap as jm; jm[\"AntiKt4EMPFlow\"] += [\"dev/flavtag/april2019/dl1/antikt4empflow/network.json\", \"dev/flavtag/april2019/dl1rmu/antikt4empflow/network.json\"] '"\
        --official\
        --osMatching\
        --noEmail\
        --inDS ${DS}\
        --outDS group.perf-flavtag.likes_flippy_puppies.${TAG}

