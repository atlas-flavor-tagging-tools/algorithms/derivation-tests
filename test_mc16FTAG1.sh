#!/bin/sh

# art-include: 21.2/AthDerivation
# art-description: DAOD building FTAG1 mc16
# art-type: grid
# art-output: *.pool.root
# art-output: checkFile.txt
# art-output: checkxAOD.txt

set -e

Reco_tf.py --inputAODFile /cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/DerivationFrameworkART/AOD.14795494._005958.pool.root.1 --outputDAODFile art-hack.pool.root --reductionConf FTAG1 --maxEvents 100 --preExec 'rec.doApplyAODFix.set_Value_and_Lock(True);from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = "BTagCalibRUN12-08-49"; BTaggingFlags.Do2019Retraining = True; from BTagging.JetCollectionToTrainingMaps import postTagDL2JetToTrainingMap as jm; jm["AntiKt4EMPFlow"] += ["dev/flavtag/april2019/dl1/antikt4empflow/network.json", "dev/flavtag/april2019/dl1rmu/antikt4empflow/network.json"]; from BTagging.JetCollectionToTrainingMaps import preTagDL2JetToTrainingMap as jm; jm["AntiKt4EMPFlow"] += ["dev/flavtag/april2019/smt/antikt4empflow/extended-hybrid-fix.json"] ' --passThrough True
