Example derivation test scripts
===============================

You can use `test_mc16FTAG1.sh` to submit a local job over 10 events.

If you've set up the grid (with ftag group production rights) you can
also use `submit-retrain-test.sh` to submit a grid job.
